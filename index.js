const express = require('express');
const app = express();
const cloudinaryStorage = require('multer-storage-cloudinary');
const cloudinary = require('cloudinary');
const multer = require('multer');

cloudinary.config({
  cloud_name: 'travelmax',
  api_key: '655948893839546',
  api_secret: 'oVku48dke7RilKRSVyfx6J5HZ5o'
})
//STORAGE IMAGES
const storage = cloudinaryStorage({
  cloudinary,
  folder: 'Images',
  allowedFormats: ['jpg', 'png'],
  transformation: [{ width: 500, height: 500, crop: 'limit' }],
  filename: function (req, file, cb) {
    cb(undefined, file.public_id);
}
});

const parser = multer({ storage });


//STORAGE FILE TYPE PDF
const storagePdf = cloudinaryStorage({
  cloudinary,
  folder: 'Documents',
  allowedFormats: ['pdf'],
  filename: function (req, file, cb) {
    cb(undefined, file.public_id);
}
});

const parserPdf = multer({ storagePdf });



//GET FILES
app.get('/',parser.array('image'),function (req, res) {
  console.log(req.files)
});




//UPLOAD IMAGE
app.post('/upload/image',parser.array('image',10), function (req, res) {
  console.log(req.files)
  const image = {};
  image.url = req.files[0].secure_url;
  image.id = req.files[0].public_id;
});




//UPDATEIMAGE
app.put('/update/image/',parser.array('image',10), function (req, res) {
  cloudinary.v2.uploader.explicit('image',
  { public_id: "Image/eftw47wgsa1gk3v89ple",
    type:'authenticated',
    invalidate: true,
    moderation_status: "approved"},
    function(error, result) {console.log(result, error); });
});





//UPLOAD PDF
app.post('/upload/file',parserPdf.array('file',10), function (req, res) {
  console.log(req.files)
  const file = {};
  file.url = req.files[0].secure_url;
  file.id = req.files[0].public_id;
});


// //UPDATE FILES
  // app.put('/update/files/:url', function(req,res){
  //   URL = "http://res.cloudinary.com/travelmax/image/upload/v1575320733/Images/wynvszglsu4sd6yukywv.jpg'"
  //     cloudinary.uploader.explicit(URL, { type: "fetch", invalidate: true }, function(result) {
  //     console.log(result)
  //   });
  // })



//DELETE IMAGE
app.delete(`/delete/files/:public_id`, function (req,res){
  resources_for_deletion = 'Images/htanqjjcriinonnd8t1w' //public_id in cloudinary
  cloudinary.api.delete_resources(resources_for_deletion,function(err,result) { 
    console.log(result,err)
  })
})






app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});